const TableName = process.env.TABLE_NAME;
const appId = parseInt(process.env.APP_ID);
// You'll need to call dynamoClient methods to envoke CRUD operations on the DynamoDB table
const dynamoClient = require("../db");

// The apps routes will uses these service methods to interact with our DynamoDB Table
module.exports = class ListAppService {
  generateParams = () => {
    return {
      TableName,
      Key: { id: appId },
    };
  };

  async getListData() {
    try {
      const listData = await dynamoClient
        .get(this.generateParams())
        .promise();
      return listData.Item;
    } catch (error) {
      return error;
    }
  }

  async getTitle() {
    try {
      const title = await dynamoClient
        .get(this.generateParams())
        .promise();
      return title.Item.title;

    } catch (error) {
      return error;
    }
  }

  async changeTitle(title) {
    try {
      let params = this.generateParams();
      params.UpdateExpression = 'set title = :newtitle';
      params.ExpressionAttributeValues = { ':newtitle': title };

      await dynamoClient
        .update(params)
        .promise()


      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise()

      return returnValue.Item.title;

    } catch (error) {
      return error;
    }
  }

  async getList() {
    try {
      const list = await dynamoClient
        .get(this.generateParams())
        .promise();
      return list.Item.items;

    } catch (error) {
      return error;
    }
  }

  async addToList(item) {
    try {
      let params = this.generateParams();
      params.UpdateExpression = 'set #i = list_append(#i,:newItem)';
      params.ExpressionAttributeNames = { '#i': 'items' };
      params.ExpressionAttributeValues = { ':newItem': [item] };

      await dynamoClient
        .update(params)
        .promise()

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise()

      return returnValue.Item.items;

    } catch (error) {
      return error;
    }
  }

  async updateItem(index, name) {
    try {
      let params = this.generateParams();

      params.UpdateExpression = 'set #i[' + index + '] = :x';
      params.ExpressionAttributeNames = { '#i': 'items' };
      params.ExpressionAttributeValues = { ':x': {'name' : name }};

      console.log(params)

      await dynamoClient
        .update(params)
        .promise()

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise()

        console.log(returnValue)

      return returnValue.Item.items;

    } catch (error) {
      return error;
    }
  }

  async deleteItem(index) {
    try {
      let params = this.generateParams();

      params.UpdateExpression = 'remove #i[' + index + ']';
      params.ExpressionAttributeNames = { '#i': 'items' };

      console.log(params)

      await dynamoClient
        .update(params)
        .promise()

      const returnValue = await dynamoClient
        .get(this.generateParams())
        .promise()

        console.log(returnValue)

      return returnValue.Item.items;

    } catch (error) {
      return error;
    }
  }
};
